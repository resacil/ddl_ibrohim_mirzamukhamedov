--Create Database and Schema--

CREATE DATABASE LibraryDB;
USE LibraryDB;

CREATE SCHEMA LibrarySchema;

--Create Tables--
CREATE TABLE LibrarySchema.Books (
  book_id INT PRIMARY KEY AUTO_INCREMENT,
  title VARCHAR(255) NOT NULL,
  author_id INT NOT NULL,
  genre VARCHAR(50),
  publish_date DATE CHECK(publish_date > '2000-01-01'),
  FOREIGN KEY (author_id) REFERENCES LibrarySchema.Authors(author_id)
);

CREATE TABLE LibrarySchema.Authors (
  author_id INT PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL
);

CREATE TABLE LibrarySchema.Members (
  member_id INT PRIMARY KEY AUTO_INCREMENT,
  first_name VARCHAR(50) NOT NULL,
  last_name VARCHAR(50) NOT NULL,
  membership_type ENUM('Standard', 'Premium') NOT NULL DEFAULT 'Standard',
  date_joined DATE NOT NULL
);

CREATE TABLE LibrarySchema.Loans (
  loan_id INT PRIMARY KEY AUTO_INCREMENT,
  book_id INT NOT NULL,
  member_id INT NOT NULL,
  loan_date DATE NOT NULL,
  return_date DATE CHECK(return_date > loan_date),
  FOREIGN KEY (book_id) REFERENCES LibrarySchema.Books(book_id),
  FOREIGN KEY (member_id) REFERENCES LibrarySchema.Members(member_id)
);

--Sample Data (Insert after creating tables)--

INSERT INTO LibrarySchema.Authors (first_name, last_name) VALUES ('Jane', 'Austen'), ('J.R.R.', 'Tolkien');

INSERT INTO LibrarySchema.Books (title, author_id, genre, publish_date) VALUES 
('Pride and Prejudice', 1, 'Romance', '1813-01-28'),
('The Lord of the Rings', 2, 'Fantasy', '1954-07-29');

INSERT INTO LibrarySchema.Members (first_name, last_name, membership_type, date_joined) VALUES 
('John', 'Doe', 'Standard', '2023-12-01'),
('Jane', 'Smith', 'Premium', '2024-01-15');

**Populate Loans table after adding record_ts**
 
-- Adding record_ts--


ALTER TABLE LibrarySchema.Books ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE LibrarySchema.Authors ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE LibrarySchema.Members ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;
ALTER TABLE LibrarySchema.Loans ADD COLUMN record_ts TIMESTAMP DEFAULT CURRENT_TIMESTAMP;

-- Verify record_ts is populated for existing rows
SELECT * FROM LibrarySchema.Books;
SELECT * FROM LibrarySchema.Authors;
SELECT * FROM LibrarySchema.Members;

